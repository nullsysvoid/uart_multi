#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/wait.h>
#include <linux/tty.h>
#include <linux/tty_driver.h>
#include <linux/tty_flip.h>
#include <linux/serial.h>
#include <linux/sched.h>
#include <linux/version.h>
#include <linux/serial_core.h>
#include <asm/uaccess.h>

short pairs = 2; //Default number of pairs of devices

#define TTY0TTY_MAJOR		0	/* dynamic allocation */
#define TTY0TTY_MINOR		0

static struct tty_port *tport;
static struct tty_driver *tty0tty_tty_driver;

static int tty0tty_open(struct tty_struct *tty, struct file *file) {

	printk(KERN_DEBUG "%s - \n", __FUNCTION__);

	return 0;
}

static void tty0tty_close(struct tty_struct *tty, struct file *file) {
	printk(KERN_DEBUG "%s - \n", __FUNCTION__);
}

static int tty0tty_write(struct tty_struct *tty, const unsigned char *buffer,
		int count) {

	struct tty_struct *tty_tst;
	int index;

	printk(KERN_DEBUG "%s - \n", __FUNCTION__);

	index = tty->index;

	if(tty->port == NULL) {
		printk(KERN_DEBUG "port1 is not opened\n");
		return -ENODEV;
	}

	if((index % 2) == 0) {
		index++;
	}
	else {
		index--;
	}

	tty_tst = tty0tty_tty_driver->ttys[index];

	//port is not opened
	if(tty_tst == NULL) {
		printk(KERN_DEBUG "port2 is not opened\n");
		return -ENODEV;
	}

	//write data
	tty_insert_flip_string(tty_tst->port, buffer, count);
	tty_flip_buffer_push(tty_tst->port);

	return count;
}

static int tty0tty_write_room(struct tty_struct *tty) {
	printk(KERN_DEBUG "%s - \n", __FUNCTION__);
	return 1;
}

#define RELEVANT_IFLAG(iflag) ((iflag) & (IGNBRK|BRKINT|IGNPAR|PARMRK|INPCK))

static void tty0tty_set_termios(struct tty_struct *tty,
		struct ktermios *old_termios) {
	printk(KERN_DEBUG "%s - \n", __FUNCTION__);
}

//
static int tty0tty_tiocmget(struct tty_struct *tty) {
	printk(KERN_DEBUG "%s - \n", __FUNCTION__);
	return 0;
}

//static int tty0tty_tiocmset(struct tty_struct *tty, struct file *file,
static int tty0tty_tiocmset(struct tty_struct *tty, unsigned int set,
		unsigned int clear) {
	printk(KERN_DEBUG "%s - \n", __FUNCTION__);
	return 0;
}

static int tty0tty_ioctl(struct tty_struct *tty, unsigned int cmd,
		unsigned long arg) {
	printk(KERN_DEBUG "%s - \n", __FUNCTION__);
	return 0;
}

static struct tty_operations serial_ops = {
		.open           = tty0tty_open,
		.close          = tty0tty_close,
		.write          = tty0tty_write,
		.write_room     = tty0tty_write_room,
		.set_termios    = tty0tty_set_termios,
		.tiocmget       = tty0tty_tiocmget,
		.tiocmset       = tty0tty_tiocmset,
		.ioctl          = tty0tty_ioctl,
};

static int __init tty0tty_init(void) {
	int retval;
	int i;

	if (pairs > 128)
		pairs = 128;
	if (pairs < 1)
		pairs = 1;

	printk(KERN_DEBUG "%s - \n", __FUNCTION__);

	tport = kmalloc(2 * pairs * sizeof(struct tty_port), GFP_KERNEL);

	/* allocate the tty driver */
	tty0tty_tty_driver = alloc_tty_driver(2 * pairs);
	if (!tty0tty_tty_driver)
		return -ENOMEM;

	/* initialize the tty driver */
	tty0tty_tty_driver->owner = THIS_MODULE;
	tty0tty_tty_driver->driver_name = "tty0tty";
	tty0tty_tty_driver->name = "tnt";
	/* no more devfs subsystem */
	tty0tty_tty_driver->major = TTY0TTY_MAJOR;
	tty0tty_tty_driver->minor_start = TTY0TTY_MINOR;
	tty0tty_tty_driver->type = TTY_DRIVER_TYPE_SERIAL;
	tty0tty_tty_driver->subtype = SERIAL_TYPE_NORMAL;
	tty0tty_tty_driver->flags = TTY_DRIVER_RESET_TERMIOS | TTY_DRIVER_REAL_RAW;
	/* no more devfs subsystem */
	tty0tty_tty_driver->init_termios = tty_std_termios;
	tty0tty_tty_driver->init_termios.c_iflag = 0;
	tty0tty_tty_driver->init_termios.c_oflag = 0;
	tty0tty_tty_driver->init_termios.c_cflag = B9600 | CS8 | CREAD;
	tty0tty_tty_driver->init_termios.c_lflag = 0;
	tty0tty_tty_driver->init_termios.c_ispeed = 9600;
	tty0tty_tty_driver->init_termios.c_ospeed = 9600;

	tty_set_operations(tty0tty_tty_driver, &serial_ops);

	for (i = 0; i < 2 * pairs; i++) {
		tty_port_init(&tport[i]);
		tty_port_link_device(&tport[i], tty0tty_tty_driver, i);
	}

	retval = tty_register_driver(tty0tty_tty_driver);

	if (retval) {
		printk(KERN_ERR "failed to register tty0tty tty driver");
		put_tty_driver(tty0tty_tty_driver);
		return retval;
	}

	return retval;
}

static void __exit tty0tty_exit(void)
{
	int i;
	
	printk(KERN_DEBUG "%s - \n", __FUNCTION__);
	for (i = 0; i < 2*pairs; ++i)
    {
		tty_port_destroy(&tport[i]);
		tty_unregister_device(tty0tty_tty_driver, i);
    }
	tty_unregister_driver(tty0tty_tty_driver);
	kfree(tport);
}

module_init(tty0tty_init);
module_exit(tty0tty_exit);

MODULE_LICENSE("GPL");
